
	/***********************Share START**************************/
	

	function linkedIN(){
		var height = 600, width = 600;
	    var leftPosition, topPosition;
	    //Allow for borders.
	    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
	    //Allow for title and status bars.
	    topPosition = (window.screen.height / 2) - ((height / 2) + 50);
	    var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
	    u=location.href;
	    t=document.title;
	    window.open("https://www.linkedin.com/shareArticle?mini=true&url="+encodeURIComponent(u)+'&title='+encodeURIComponent(t)+'&source='+encodeURIComponent(u),'sharer', windowFeatures);
	    return false;
	}

	function facebook(){
		share("https://www.facebook.com/sharer/sharer.php?u=");
	}

	function twitter(){
		share("https://twitter.com/home?status="+encodeURIComponent(t) + " ");
	}

	function googlePlus(){
		share("https://plus.google.com/share?url=");
	}

	u=location.href;
	t=document.title;

	function share(link)
	{
		var height = 600, width = 600;
	    var leftPosition, topPosition;
	    //Allow for borders.
	    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
	    //Allow for title and status bars.
	    topPosition = (window.screen.height / 2) - ((height / 2) + 50);
	    var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
	    window.open(link+encodeURIComponent(u),'sharer', windowFeatures);
	    return false;
	}
	
	/***********************Share END**************************/



/*Overlay properties*/
function ghOverlay(){
	$('.overlayWrap').fadeIn('fast');
	$('body').addClass('noScroll');
	$('.overlayWrap').click(function(){
		//$(this).addClass('fadeOutDownBig');
		//$(this).removeClass('fadeOutDownBig');
	});
	$('a.closeOverlay').click(function(){
		$('.overlayData, .globalSearchWidget').hide();
		$('ul.navLinks li').removeClass('active');
		$('.overlayWrap').fadeOut('fast');
		$('body').removeClass('noScroll');
	});
};

//Featured Grid Frames
function featuredGridFrame(){
	var fgHeight = $(window).height();
	var fgWidth = $(window).width();
	var fgRow = $(window).height()/3 + 'px';
	var fgCol = $(window).width()/3 + 'px';
	
	$('.featuredGridWrap').height(fgHeight);
	$('.booksLiberaryDetail_Wrap.readOnline nav.allLinks').height(fgHeight);
	$('.featuredGridRow').height(fgRow);
	$('.featuredGridWrap').width(fgWidth);
	
	//TimeLine Height
	//$('#di-events-timeline').height(fgHeight);

	//Slider Div
	$('.sliderBg').height(fgHeight);
}

//Global Nav Expend / Collapse Effect
function gNavExpendEffect(){
	
}

/*************************** On Window Resize Start
****************************/
window.onresize = function(event) {
	//imgHeight();

	//Featured Grid Frames
	featuredGridFrame();	
	
	//Books Read Online - affix width 
	
}
/*************************** On Window Resize END
****************************/

/*************************** On Window Load Start
****************************/
/*************************** On Window Load END
****************************/

/*************************** Document ready start
****************************/
$(document).ready(function(e) {
    /* g-Share Click*/
	$('#search-btn').click(function(){
		var search_type = $('#search-type').val();
		var search_value =$('#search-church').val();
		if(search_type=="media") 
		{
			window.location.assign(webUrl+"?search=" + search_type + "&"+"searchvalue="+search_value);
		}
	});
	$('.g-Share').click(function(){
		$('ul.shareList').slideToggle('fast');
	});
	
	/* g-Language Click*/
	$('.g-Language').click(function(){
		$('ul.langList').slideToggle('fast');
	});
	
	/*gNav Click*/
	$('ul.navLinks li').click(function(e){
		$('ul.navLinks li').removeClass('active');
		$(this).addClass('active');
	});
	
	
	/*Global Menu click*/
	$('ul.navLinks li.g-Menu a').click(function(e){
		//$(this).toggleClass('active');
		$('.gNavWrap').slideToggle('fast');
		//$('body').addClass('noScroll');
		setTimeout(function(){
			$('.gHomeControlWrap').addClass('animated fadeOutDownBig hidden');
			$('.gHomeControlWrap').removeClass('fadeInUp');
		},500);
	});
	//Global Nav Expend / Collapse Effect
	$('ul.navListExpand li a').click(function(){
		var subID = $(this).attr('data-sub');
		$('ul.navListExpand li a').removeClass('selected');
		$(this).addClass('selected');
		if($(this).hasClass('noSub')){
			return;
		}
		else{
			$('.gNavAside').addClass('expended');	
		}
		$('.subNavWrap .subNavData').removeClass('show');
		$('.spinner').fadeIn('fast');
		setTimeout(function(){
			$('.subNavWrap').find('#' + subID).addClass('show');
			$('.spinner').fadeOut('fast');
		},500);
	});
	
	/*Global Menu Close*/
	$('a.closeGnav').click(function(){
		$('.gNavWrap').addClass('animated fadeOutDown');	
		$('ul.navLinks li').removeClass('active');
		//$('body').removeClass('noScroll');
		setTimeout(function(){
			$('.gNavWrap').hide();
			$('.gNavWrap').removeClass('animated fadeOutDown');	
			$('.gNavAside').removeClass('expended');	
			$('ul.navListExpand li a').removeClass('selected');
			$('.gHomeControlWrap').removeClass('animated fadeOutDownBig hidden');
			$('.gHomeControlWrap').addClass('animated fadeInUp');	
		},1000);
	});
	
	/*Global Search click*/
	$('.GlobalSearch-xs').click(function(e){
		$(this).toggleClass('active');
		ghOverlay();
		setTimeout(function(){
			$('.globalSearchWidget').fadeIn('slow');
			$('input.startTyping').val('');
			$('input.startTyping').focus();
		},800);
		
	});
	
	

	
	/************************Search start************************/
	
	$('.searchResultspinner').hide();
	
	$("#txtSearch").focus();
	
	$('#search-type').on('change', function (e) {
		var value = $('#search-type').val();
		if (value=="book")
			suggestionUrl = "http://www.dawateislami.net/solr/book/suggest?suggest=true&wt=json&suggest.q=";
		else if (value=="gallery")
			suggestionUrl = "http://www.dawateislami.net/solr/gallery/suggest?suggest=true&wt=json&suggest.q=";
		else if (value=="media")
			suggestionUrl = "http://www.dawateislami.net/solr/media/suggest?suggest=true&wt=json&suggest.q=";
		
		$('#txtSearch').val("");
		$("#pageNumber").val(1);
		cache = {};
	});
	
	var title = document.title;
	
	document.title = title;
	window.jsoncallback = function(data){
	}

		  

	/* var cache = {};
		    $( "#txtSearch" ).autocomplete({
		      minLength: 2,
		      source: function( request, response ) {
		        var term = request.term;
		        if ( term in cache ) {
		          response( cache[ term ] );
		          return;
		        }
		        
		 

		     
		        $.getJSON( suggestionUrl+term, request, function( data, status, xhr ) {
		       	var suggestions = data.suggest.mySuggester[term].suggestions;
		         var termsArrays = new Array();
		         for(var idx =0; idx< suggestions.length;idx++){
		         	termsArrays.push(suggestions[idx].term);
		         }
		         cache[ term ] = termsArrays;
		         
		         response(termsArrays);
		       //  response( data );
		       });
		      }
		    });*/
		    /* var valueItmeSelected;
		    $( "#txtSearch" ).autocomplete({
		  select: function( event, ui ) {
		  		valueItmeSelected=ui.item.value;
		  		searchContent();
		  	  //searchBookSuugestion();
		  	
		  }
		});*/
		
		/******************************Search end************************/
	
	
	/*$('.globalSearchWidget input.startTyping').on('input', function() {
		//destroy owl Carousel
		$('.searchResultWrap').fadeIn('fast');
		$('.searchResultspinner').fadeIn('fast');
		setTimeout(function(){
			
			//Init owl Carousel
			//Search Result list
			var owl = $('#searchResult');
			owl.owlCarousel({
				loop:true,
				nav:false,
				margin:10,
				responsive:{
					0:{
						items:1
					},
					600:{
						items:3
					},            
					960:{
						items:5
					},
					1200:{
						items:6
					}
				}
			});
			owl.on('mousewheel', '.owl-stage', function (e) {
				if (e.deltaY>0) {
					owl.trigger('next.owl');
				} else {
					owl.trigger('prev.owl');
				}
				e.preventDefault();
			});
			
			$('.searchResultspinner').fadeOut('fast');
		},1000);
		if ( this.value.length == 0) {
			$('.searchResultWrap').fadeOut('fast');
		}
	});*/
	
	
	//More Options - Search Result
		$('ul.SearchResult-List > li a.moreOption').click(function(){
			//$('.searchResult-OptionList').fadeIn('fast');
			alert('polka');
		});
		
	/*Page Content Search click*/
	$('ul.searchPageContent li a').click(function(e){
		$(this).toggleClass('active');
		$('.spcOpen').slideToggle();
	});
	
	//Featured Grid Frames
	featuredGridFrame();
	
	
	//Video Slider Box
	$('.mediaSlider_Wrap ul.optionList li a').on('click', function(){
		if ($(this).closest('.mediaSlider_Wrap').find('.boxvidSumRight').hasClass('openPallate')){
			return (true);
		}
		if ($(this).hasClass('extPop')){
			return (true);
		}
		$('.currentVidDesc').hide();
		$(this).closest('.mediaSlider_Wrap').find('.boxvidSumRight').toggleClass('openPallate');
	});
	//Close
	$('.mediaSlider_Wrap .boxvidSumRight .closeX').on('click', function(){
		$('ul.optionList li').removeClass('active');
		$(this).closest('.mediaSlider_Wrap').find('.boxvidSumRight').removeClass('openPallate');
		$('.boxvidSumRight').find('.currentVidDesc').fadeIn('fast');
	});
	
	//Video List Que Box :Hover
	$('.mediaList_Videos li ul.QueOptionList li a').on('click', function(){
		if ($(this).closest('.boxvidSumRight').hasClass('openPallate')){
			return (true);
		}
		$(this).closest('.boxvidSumRight').toggleClass('openPallate');
	});
	//Close
	$('.mediaList_Videos .boxvidSumRight .closeX').on('click', function(){
		$(this).parent('li').removeClass('active');
		$(this).closest('.boxvidSumRight').removeClass('openPallate');
	});
	
	
	//Media Liberary Aside Box Accordian
	$('.asideBox h2.aside-ListTitle').click(function(){
		$(this).siblings('nav').slideToggle('fast');
	});
	
	//Media Detail Right Column Height
	var rcHeight = $(window).height();
	$('.mediaLiberaryDetail_Wrap ul.recommendedVideosList').height(rcHeight);
	
});

/*************************** Document ready End
****************************/

/*************************** Slider Semi Carousel
****************************/
$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 10) {
	$('.gHomeControlWrap').addClass('animated fadeOutDownBig hidden');
	$('.gHomeControlWrap').removeClass('fadeInUp');
	
	//footer bar
	//$('.home_Wrap footer').addClass('fixed');
  } else {
	$('.gHomeControlWrap').removeClass('fadeOutDownBig hidden');
	$('.gHomeControlWrap').addClass('animated fadeInUp');

	//footer bar
	//$('.home_Wrap footer').removeClass('fixed');
  }
});
/*************************** Slider Semi Carousel End
****************************/


/*************************** Footer Bar Move
****************************/
/*$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 700) {
	$('.home_Wrap footer').addClass('fixed');
  } else {
	$('.home_Wrap footer').removeClass('fixed');
  }
});*/

/*************************** Footer Bar Move End
****************************/

/**************************Start media Load Button Spanner
***************************/
$('.loadMoremediaListBtn').click(function() {
	document.getElementById("button.loadMoremediaListBtn").innerHTML = "New text!";
	
});


function loadMoreBooks() {

		
	var serializedData = $('#filterForm').serialize();

	$.ajax({
		url : appContext + "/ajax",
		type : 'GET',
		data : serializedData + "&ajax=true",
		success : function(data) {

			$('#filterForm').remove();
			$("#loadMoreNews").append(data);
			$('#loadMoreNewsBtn').attr('disabled', false);

		},
		error : function() {
			console.log("Some error there.");
		}
	});
	}

$("#loadMoreNewsBtn").click(function(event) {
	loadMoreBooks();
	$('#loadMoreNewsBtn').attr('disabled', true);
});
  