<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Services;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class BaseApiCall
{

	public function __construct() {
        ini_set("set_max_exec_time", 90000000);
    }

    public function get($url,$header=[])
    {
     
        try {
            $client = new Client();
            $res = $client->request('GET', $url, [
                 'headers' => $header,
            ]);

        }catch (\Exception $e)
        {
           //dd($e->getMessage());
            return false;
        }

        return $res->getBody()->getContents();
    }

     public function getMulti($requestData)
    {
        $requests = [];
        $client = new Client();
        
        foreach ($requestData as $key=> $reqData):
            $requests[$key] = new Request('GET',$reqData['url'],$reqData['header']);
        endforeach;
        
       $responses = Pool::batch($client, $requests, ['concurrency' => 60]);

       $response = [];
      foreach ($responses as $keyres=> $res) {
       $response[$keyres] =  $res->getBody()->getContents();
        }
       
        return $response;
    }

    public function post($url,$header,$data)
    {
        //dd($header);
        try{
        $client = new Client();
        
        $res = $client->request('POST',$url, [
                'headers' =>$header,
                'form_params' => $data,
            ]);
         return $res->getBody()->getContents();
        } catch (\Exception $e)
        {
           // dd($e->getMessage());
             return false;
        }
    }

}