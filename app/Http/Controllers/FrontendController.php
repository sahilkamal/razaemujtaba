<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ExternalApiFunction;

class FrontendController extends Controller
{
    //
	public $objYtb ;
    public function __construct(){
    	$this->objYtb = new ExternalApiFunction();	
    }
    public function index(){

    	$videosResult  = $this->objYtb->getYoutubeVideos();
    	//$videoCollection  = collect($videosResult);	
    	//dd($videosResult);
    	/*foreach($videosResult as $videos){
    		dump(@$videos->id->videoId);
    	}
    	dd("s");*/
    	return view('frontend.app',compact('videosResult'));
    }
}
