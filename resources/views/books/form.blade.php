{{ Form::open([
    'url' => '/customers'.(@$customer?'/'.@$customer->id:''),
    'class' => 'form-horizontal',
    'method' => @$customer?'PATCH':'POST',
     'data-toggle' => 'validator'
]) }}

    {{ Form::bsText(
            'first_name', 'First Name',['required'=>'required'] , @$customer->first_name
    ) }}
    {{ Form::bsText(
             'last_name', 'Last Name',['required'=>'required'] , @$customer->last_name
    ) }}
    {{ Form::bsText(
            'email', 'Email',['required'=>'required'] , @$customer->phone
        ) }}
    {{ Form::bsText(
            'phone', 'Phone',['required'=>'required'] , @$customer->phone
        ) }}
    {{ Form::bsText(
            'company_name', 'Company Name',['required'=>'required'] , @$customer->company_name
        ) }}
    {{ Form::bsText(
          'company_mobile', 'Company Mobile',['required'=>'required'] , @$customer->company_mobile
    ) }}
    {{ Form::bsText(
          'company_email', 'Company Email',['required'=>'required'] , @$customer->company_email
     ) }}

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-flat btn-primary">
                <i class="fa fa-btn fa-plus"></i>
                {{ @$customer?'Update':'Create' }}
            </button>
            <a
                href="{{ url('customers') }}"
                class="btn btn-flat btn-warning"
            >
                <i class="fa fa-btn fa-times"></i> Cancel
            </a>
        </div>
    </div>
{{ Form::close() }}