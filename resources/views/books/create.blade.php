@extends('layouts.app')
@section('title', 'Add New Customer')

@section('content-header')
    <h1>New Customer <small>Add New Customer</small></h1>
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            <div class="panel">
                
                <div class="panel-body">

                    
                    @include('customers.form')

                </div>

            </div>                
            </div>
        </div>
    </div>
@endsection
