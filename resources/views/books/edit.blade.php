@extends('layouts.app')
@section('title', 'Edit License Group')

@section('content-header')
    <h1>Edit License Group <small>Edit License Group</small></h1>
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            <div class="panel">
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger alert-dismissable" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria=hidden="true">&times;</span></button>
                            You have an error:
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @include('groups.form')
                </div>
            </div>                
            </div>
        </div>
    </div>
@endsection
