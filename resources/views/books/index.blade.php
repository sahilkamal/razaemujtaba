@extends('layouts.app')
@section('title', 'Customer List')

@section('style')
{{ Html::style(
    '/assets/DataTables/DataTables-1.10.12/css/dataTables.bootstrap.css'
) }}
<style>
.table.table-field-dictionary tr td {
    vertical-align: top;
    max-width: 250px;
    word-wrap: break-word;
}
.icons{
    position: absolute;
    right: 7px;
    top: 2px;
}

</style>
@endsection

@section('content-header')
<h1>
    Customer List <small>List of Customers</small>
    <a
        href="{{ route('customers.create') }}"
        class="btn fa-btn btn-primary btn-flat pull-right"
    ><i class="fa fa-plus-circle"></i> Add New Customer</a>
    <div class="clearfix"></div>
</h1>
@endsection

@section('content')
<div class="col-xs-12">
    <div class="box">
        <div class="box-body">
            @if (session('success'))
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button
                        type="button"
                        class="close"
                        data-dismiss="alert"
                        aria-label="Close"
                    ><span aria=hidden="true">&times;</span></button>
                    {{ session('success') }}
                </div>
            @endif

            <table class="table table-hover table-field-dictionary" id='datatable'>
            <colgroup>
                <col width="1"></col>
                <col></col>
                <col class="actions"></col>
            </colgroup>
            <thead>
                <tr>
                    <th></th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Customer Email</th>
                    <th>Phone</th>
                    <th>Company Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
            </table>
    </div>
</div>
</div>
@endsection
@section('script')
{{ Html::script('/assets/DataTables/DataTables-1.10.12/js/jquery.dataTables.js') }}
{{ Html::script('/assets/DataTables/DataTables-1.10.12/js/dataTables.bootstrap.js') }}
<script>
    $(function () {

      var datatable_url = "{{route('customers.getAjaxCustomers')}}";
      var datatable_columns = [
                    {data: 'multi_select', name: 'multi_select', width: '3%',orderable: false, searchable: false},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'company_name', name: 'company_name'},
                    {data: 'action', name: 'action', width: '10%',orderable: false, searchable: false}
            ];

        $('#datatable').DataTable({
            oLanguage: { sProcessing: '<img src="'+ BASEURL +'/assets/img/bx_loader.gif">' },
            processing: true,
            serverSide: true,
            ajax: {
                url: datatable_url,
                data : function(d){
                    console.log(d);
                    if($(".filter_by_type").val() != ''){
                        //d.columns[2]['search']['value'] = $(".filter_by_store option:selected").text();
                    }
                }
            },
            columns: datatable_columns,
            "order": []
        });
        var reload_datatable = $("#datatable").dataTable( { bRetrieve : true } );
        $(document).on('change', '.filter_by_type', function (e) {
            reload_datatable.fnDraw();
        });

        $("#datatable_length").append('<button href="{{ route("customers.deleteMultiRecords") }}" class="btn btn-sm btn-danger btn-multi-delete" style="margin-left: 20px;display:none;">Delete</button>');
    });
</script>
@endsection
