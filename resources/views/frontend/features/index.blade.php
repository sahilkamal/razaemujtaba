<div class="row">
        <div class="col-md-12">
          <h3 class="color-scheme-1-txt"><i class="fa fa-puzzle-piece"></i> Featured Sections</h3>
        </div>
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover"  href="javascript:void(0)" target="_blank">
          <div class="panel-body card">
            <div class="card-image">
              <img src="{{asset('assets/frontend/img/featured/quran-e-kareem.jpg')}}" class="img-block-responsive" alt="Quran E Kareem" />
            </div>
          </div>
          <div class="panel-footer color-scheme-1-bg">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">Quran E Kareem</span>
            </span>
          </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover"  href="javascript:void(0)">
          <div class="panel-body card">
            <div class="card-image">
              <img src="{{asset('assets/frontend/img/featured/about-sarkar.jpg')}}" class="img-block-responsive" alt="Holy Prophet صلی اللہ علیہ وسلم" />
            </div>
          </div>
          <div class="panel-footer color-scheme-1-bg">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">The Holy Prophet صلی اللہ علیہ واٰلہ وسلم​</span>
            </span>
          </div>
          </a>
        </div>
       
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover"  href="javascript:void(0)">
          <div class="panel-body card">
            <div class="card-image">
              <img src="{{asset('assets/frontend/img/featured/media-library.jpg')}}" class="img-block-responsive" alt="Media Library" />
            </div>
          </div>
          <div class="panel-footer color-scheme-1-bg">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">Media Library</span>
            </span>
          </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover"  href="javascript:void(0)">
          <div class="panel-body card">
            <div class="card-image">
              <img src="https://i.ytimg.com/vi/DtvlroYYRYM/hqdefault.jpg" class="img-block-responsive" alt="Ameer e Ahle Sunnat" />
            </div>
          </div>
          <div class="panel-footer color-scheme-1-bg">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">Ameer e Ahle Sunnat</span>
            </span>
          </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover"  href="javascript:void(0)">
          <div class="panel-body card">
            <div class="card-image">
              <img src="{{asset('assets/frontend/img/featured/book-library.jpg')}}" class="img-block-responsive" alt="Book Library" />
            </div>
          </div>
          <div class="panel-footer">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">Book Library</span>
            </span>
          </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover" href="javascript:void(0)">
          <div class="panel-body card">
            <div class="card-image">
              <img src="{{asset('assets/frontend/img/featured/image-gallery.jpg')}}" class="img-block-responsive" alt="Image Gallery" />
            </div>
          </div>
          <div class="panel-footer color-scheme-1-bg">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">Image Gallery</span>
            </span>
          </div>
          </a>
        </div>
        </div>