 <div class="row">
          <div class="col-md-12">
            <h3><i class="fa fa-book color-scheme-2-txt"></i> Featured Books</h3>
          </div>
          <div class="clearfix"></div>
                  <div class="col-md-3 col-sm-3 col-xs-6 book-item margin-tb-10">
            <a href="javascript:void(0)" title="Nekiyan Chupao"><img src="https://data2.dawateislami.net/Data/Books/Read/ur/2016/1592/bt1592.jpg" alt="Nekiyan Chupao" class="img-responsive img-center">
              <h4 class="text-center color-scheme-2-txt">Nekiyan Chupao</h4></a>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 book-item margin-tb-10">
            <a href="javascript:void(0)" title="Waliullah Ki Pehchan"><img src="https://data2.dawateislami.net/Data/Books/Read/ur/2016/1496/bt1496.jpg" alt="Waliullah Ki Pehchan" class="img-responsive img-center">
              <h4 class="text-center color-scheme-2-txt">Waliullah Ki Pehchan</h4></a>
          </div>

                  <div class="col-md-3 col-sm-3 col-xs-6 book-item margin-tb-10">
            <a href="javascript:void(0)" title="Rafiq-e-Safar Kesa Ho?"><img src="https://data2.dawateislami.net/Data/Books/Read/ur/2018/1742/bt1742.jpg" alt="Rafiq-e-Safar Kesa Ho?" class="img-responsive img-center">
              <h4 class="text-center color-scheme-2-txt">Rafiq-e-Safar Kesa Ho?</h4></a>
          </div>

                  <div class="col-md-3 col-sm-3 col-xs-6 book-item margin-tb-10">
            <a href="javascript:void(0)" title="Mufti e Dawateislami"><img src="https://data2.dawateislami.net/Data/Books/Read/ur/2006/158/bt158.jpg" alt="Mufti e Dawateislami" class="img-responsive img-center">
              <h4 class="text-center color-scheme-2-txt">Mufti e Dawateislami</h4></a>
      </div>       
      <div class="clearfix"></div>
          <div class="col-md-12">
            <ul class="pager">
              <li><a href="javascript:void(0)" class="btn-lg">Latest Books <span aria-hidden="true">&rarr;</span></a></li>
            </ul>
          </div>
        </div>