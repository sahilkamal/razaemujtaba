<div class="row">
     <div class="col-md-12 col-sm-12 box-nav-gray">
          <img src="https://www.dawateislami.net/images/khutba.svg" class="khutba-slogan img-responsive" alt="Dawat-e-Islami">
        </div>
        <div class="container">
          <div class="row hidden-xs hidden-is hidden-sm header-banner">
            <div class="col-md-2 col-sm-2">
              <img src="{{asset('assets/frontend/images/logo_0.png')}}" alt="idara raza e mujtaba" class="img-responsive logo-header">
            </div>
            <div class="col-md-8 col-sm-8">
              <!-- <img src="https://www.dawateislami.net/images/motto-urdu-30px.svg" class="img-responsive svg img-center motto-urdu m-urdu" alt="Madani Maqsad">
              <img src="https://www.dawateislami.net/images/motto-en-30p-2.svg" class="img-responsive svg img-center motto-eng m-en hidden" alt="Madani Maqsad"> -->
            </div>
            <div class="col-md-2 col-sm-2">
        <p class="text-center"><span class="h2">19</span> <br>Muharram ul Haram<br>1440 Hijri</p>
        <!-- <p class="text-center"><span class="h2">30</span> <br>Muharram ul Haram<br>1440 Hijri</p> -->
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row nav-block box-nav-gray">
            <nav class="navbar navbar-main navbar-inverse" role="navigation">
              <div class='cont-far'>
                <div class="row">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hidden-md hidden-lg" href="#">
                      <!-- <img src="{{asset('assets/frontend/images/logo-white.png')}}" alt="logo" class="img-responsive logo"> -->
                    </a>
                    <select class="form-control hidden-md hidden-lg" style="width: 30%;color: black;margin-left: 20%;" id="site_lang" name="site_lang">
                      <option value="">Global</option>
                    </select>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="navbar">
              <ul class="nav navbar-nav">
                <li class="active"><a href="javascript:void(0)">Home</a></li>
                <li><a href="javascript:void(0)" target="_blank">Al-Quran</a></li>
                      <li><a href="javascript:void(0)">Books</a></li>
                <li> <a href="javascript:void(0)">Media</a> </li>
                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Donation<i class="fa fa-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                          <li><a href="javascript:void(0)">Why Donate to us?</a></li>
                <li><a href="javascript:void(0)">Donate Now</a></li>
                        </ul>
                </li>
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Departments <i class="fa fa-chevron-down"></i></a>
              <ul class="dropdown-menu">
                  <li><a href="javascript:void(0)">Farz Uloom</a></li>
                  <li><a href="javascript:void(0)">Islamic Sisters</a></li>
                  <li><a href="javascript:void(0)">Madrasa-tul-Madina Online</a></li>
                  <li><a href="javascript:void(0)">Hajj-o-Umrah</a></li>
                 <!-- <li><a href="https://departments.dawateislami.net/toqeet/">Majlis Tauqeet</a></li>-->
                  <li><a href="javascript:void(0)">Tajheez-o-Takfeen</a></li>
                  <li><a href="javascript:void(0)">Tawizat-e-Attaria</a></li>
                  <li><a href="javascript:void(0)">Ahkam-e-Hajj</a></li>
                  <li><a href="javascript:void(0)">Social Media</a></li>
              </ul>
              </li>
                   <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">More<i class="fa fa-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)">Social Activities</a></li>
                            <li><a href="/gallery/">Gallery</a></li>
                            <li><a href="javascript:void(0)">Downloads</a></li>
                            <li><a href="javascript:void(0)">Location</a></li>
                            <li class="visible-sm"><a href="javascript:void(0)">Prayer Timings</a></li>
                            <li class="visible-sm"><a href="javascript:void(0)">Ruhani Ilaj</a></li>
                        </ul>
                      </li>
                    </ul>
                <ul class="nav navbar-nav" style="float:right;margin-top:3px;">
                  <li>
                  <select class="form-control hidden-xs hidden-sm" id="site_lang" name="site_lang" >
                    <option value="">Global</option>
                    <!-- <option value="/ar">العربية</option> -->
                  </select>
                  </li>
                </ul>
                </div><!-- /.navbar-collapse -->
              </div>
            </div>
          </nav>
        </div>
      <div class="row quicklinks">
          <nav class="navbar navbar-inverse ql-navbar ql-opaque">
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12 hidden-xs ">
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                </div>
              </div>
            </div>
          </nav>
       </div>
    </div>     
<script>
  function loadSite(site) {
    if(site != null && site.trim().length > 0) {
      window.location = window.location.origin + site;
    }
  }
 </script>
   </div>