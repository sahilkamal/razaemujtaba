<div class="col-md-6 col-sm-5 col-xs-12 border-b-2">
      <ul id="social-service">
            <li><a href="https://twitter.com/idararazaemujtaba" target="_blank"><img src="{{asset('assets/frontend/images/twitter.png')}}" alt="Share twitter"></a></li>
            <li><a href="https://plus.google.com/+razaemujtaba" target="_blank"><img src="{{asset('assets/frontend/images/google.png')}}" alt="Share google"></a></li>
            <li><a href="https://www.facebook.com/dawateislami.net" target="_blank"><img src="{{asset('assets/frontend/images/facebook.png')}}" alt="Share facebook"></a></li>
      </ul>
        </div>
        <div class="col-md-6 col-sm-7 col-xs-12 connect border-b-2">
      <h4 class="col-md-offset-3 col-sm-offset-2 color-scheme-2-txt">Connect to Idara e Razae Mujtaba</h4>
        <div class="col-md-6 col-sm-6 col-xs-12">
          
          <span><strong>Phone: </strong><a href="tel:+922134921388">(+92)21-34921388-93</a></span> <br>
          <span><strong>UAN: </strong><a href="tel:+9221111252692">(+92)21-111-25-26-92</a></span> <br>
          <span><strong>Email: </strong><a href="mailto:support@razaemujtaba.com">support@razaemujtaba.com</a></span>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
        
          <address>
             <strong>Address: </strong>  189 R.B rasoolpul 
                 <br> Chak jhumra (69.14 mi)<br>
                Faisalabad, <br>
                Pakistan <br>
              </address>
        </div>
        </div>