<!DOCTYPE html>
<html lang="en">
<head>  
  <title>Idara Raza e Mujtaba</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">  


<link rel="stylesheet" href="{{asset('assets/frontend/css/main.css')}}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
<link rel="stylesheet" href="{{asset('assets/frontend/css/jquery-ui.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css" integrity="sha256-ev+XS9lVA6/6vEe/p9pncQjsHB6g9UtAZYFLNViXxAA=" crossorigin="anonymous" />
<link rel="stylesheet" href="{{asset('assets/frontend/css/common.css')}}">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jssor-slider/27.5.0/jssor.slider.min.js"crossorigin="anonymous"></script>
<script src="{{asset('assets/frontend/js/common.js')}}"></script>

 </head>
<body>
<div class="preloader" style="display: none">
      <div class="lds-ripple">
          <div class="lds-pos"></div>
          <div class="" lass="lds-pos"></div>
      </div>
  </div> 
<div class="container-fluid">
      @include('frontend.header')
 </div>     
<div class="container-fluid content">
<!-- <Slider start>-->                 
<div class="container">
     <section class="row">
         @include('frontend.sliders.index')
         <!-- start  -->
         @include('frontend.videos.index')
     </section>
    	 @include('frontend.programs.index')
            
    </section>
   </section>
 </div>
<!-- <Latest Book start>-->
<section class="row box-green books" data-type="background" data-speed="10" style="max-width:  none;">
   <div class="container">
       @include('frontend.books.index')
      </div>
</section>   
<!-- <Featured start>-->
<section class="row featured">
      <div class="container">
        @include('frontend.features.index')
      </div>
    </section>        
<!-- <About Us start>-->
<section class="row about">
        @include('frontend.about.index')
</section>      
<!-- <Footer start>-->
<section class="row footer"> 
 	    @include('frontend.footer')  
 </section>
<!-- </Footer end>--> 
<section class="row copyright">
      <div class="col-md-12">
        <h5 class="text-center">Copyright &copy; <span>2018</span> by Idara Raza e Mujtaba</h5>
      </div>
</section>
<!-- end -->
</div>

<script src="{{asset('assets/frontend/js/layout.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery-ui.js')}}"></script>
<script src="{{asset('assets/frontend/js/storyjs-embed.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.serializejson.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js" integrity="sha256-nHmCK+HOPMPezzS3ky9VKznMWH4sW4keT8HrMaDNbYo=" crossorigin="anonymous"></script>
<script src="{{asset('assets/frontend/js/main.js')}}"></script>

<!-- <script language="javascript" type="text/javascript">
     $(window).load(function() {
     $('.preloader').hide();
  });
</script> -->
</body>
</html>
  

