 <div class="row">
        <div class="col-md-6 col-sm-offset-5">
              <h3>Latest Programs </h3>
        </div>
     </div>
     <section class="row">
        <div class="col-md-6 col-sm-12 col-is-12 col-xs-12">
            <div class="panel panel-default card-lg">
            <div class="panel-body card">
              <div class="card-image">
                <a href="javascript:void(0)"><img src="{{asset('assets/frontend/images/qari_sb_01.png')}}" class="img-block-responsive wide"></a>
                </div>
              </div>
              <div class="panel-footer">
                <span class="card-description h5"><a class="link-white-hover" href="javascript:void(0)">Mukhtalif Shehron Ka Jadwal</a>
              </span>
            </div>
            </div>
          </div>
              <div class="col-md-3 col-sm-6 col-is-6 col-xs-12" >
              <div class="panel panel-default card-md">
              <div class="panel-body card">
                <div class="card-image"><a href="javascript:void(0)"><img src="{{asset('assets/frontend/images/sahi_bukhari.jpg')}}" class="img-block-responsive wide"></a>
                </div>
              </div>
              <div class="panel-footer">
                <span class="card-description h5"><a class="link-white-hover" href="https://www.dawateislami.net/medialibrary/14428">Sahih al-Bukhari</a></span>
              </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-is-6 col-xs-12">
              <div class="panel panel-default card-md">
              <div class="panel-body card">
              <div class="card-image"><a href="javascript:void(0)"><img src="{{asset('assets/frontend/images/qurbani_tofa.jpg')}}" class="img-block-responsive wide"></a>
                </div>
              </div>
              <div class="panel-footer">
                <span class="card-description h5"><a class="link-white-hover" href="javascript:void(0)">Qurbani ka Tufa</a></span>
              </div>
              </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-is-6 col-xs-12">
              <div class="panel panel-default card-md">
              <div class="panel-body card">
                <div class="card-image"><a href="javascript:void(0)"><img src="https://i.ytimg.com/vi/TjthxY4gwSM/hqdefault.jpg" class="img-block-responsive wide"></a>
                </div>
              </div>
              <div class="panel-footer">
                <span class="card-description h5"><a class="link-white-hover" href="javascript:void(0)">Wo Aik Hi Hay Bas</a></span>
              </div>
              </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-is-6 col-xs-12">
              <div class="panel panel-default card-md">
              <div class="panel-body card">
                <div class="card-image"><a href="javascript:void(0)"><img src= "https://data2.dawateislami.net/static/images/main-website/banners/515-prayer-time-applicatin.jpg" class="img-block-responsive wide"></a>
                </div>
              </div>
              <div class="panel-footer">
                <span class="card-description h5"><a class="link-white-hover" href="javascript:void(0)">Prayer Times Application</a></span>
              </div>
              </div>
            </div>