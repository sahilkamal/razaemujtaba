<!DOCTYPE html>
<html lang="en">
<head>  
  <title>Idara Raza e Mujtaba</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">  

<link rel="icon" type="image/x-icon" href="https://www.dawateislami.net/images/favicon.ico">
<link rel="icon" type="image/x-icon" href="{{asset('assets/frontend/images/favicon.ico')}}">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/frontend/images/favicon.ico')}}">
<link rel="stylesheet" href="{{asset('assets/frontend/css/main.css')}}">
<link rel="stylesheet" href="{{asset('assets/frontend/css/timeline.css')}}">  
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
<link rel="stylesheet" href="{{asset('assets/frontend/css/jquery-ui.css')}}">
<link rel="stylesheet" href="{{asset('assets/frontend/css/owl.theme.default.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" integrity="sha256-fDncdclXlALqR3HOO34OGHxek91q8ApmD3gGldM+Rng=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css" integrity="sha256-ev+XS9lVA6/6vEe/p9pncQjsHB6g9UtAZYFLNViXxAA=" crossorigin="anonymous" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jssor-slider/27.5.0/jssor.slider.min.js"crossorigin="anonymous"></script>
<style type="text/css">
  /*.lSPager {
    display: none !important; 
  }*/
</style>


    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:500,$Delay:30,$Cols:8,$Rows:4,$Clip:15,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2049,$Easing:$Jease$.$OutQuad},
              {$Duration:500,$Delay:80,$Cols:8,$Rows:4,$Clip:15,$SlideOut:true,$Easing:$Jease$.$OutQuad},
              {$Duration:1000,x:-0.2,$Delay:40,$Cols:12,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Assembly:260,$Easing:{$Left:$Jease$.$InOutExpo,$Opacity:$Jease$.$InOutQuad},$Opacity:2,$Outside:true,$Round:{$Top:0.5}},
              {$Duration:2000,y:-1,$Delay:60,$Cols:15,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:$Jease$.$OutJump,$Round:{$Top:1.5}},
              {$Duration:1200,x:0.2,y:-0.1,$Delay:20,$Cols:8,$Rows:4,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:260,$Easing:{$Left:$Jease$.$InWave,$Top:$Jease$.$InWave,$Clip:$Jease$.$OutQuad},$Round:{$Left:1.3,$Top:2.5}}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 10000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                    //jssor_1_slider.$ScaleHeight(643);
                }
                else {
                    window.setTimeout(ScaleSlider, 10);
                }
            }

            ScaleSlider();


            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /* jssor slider loading skin spin css */
        left-arrow: {visibility: hidden}
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }

            to {
                transform: rotate(360deg);
            }
        }
        .jssorb053 .i {position:absolute;cursor:pointer;}
        .jssorb053 .i .b {fill:#fff;fill-opacity:0.5;}
        .jssorb053 .i:hover .b {fill-opacity:.7;}
        .jssorb053 .iav .b {fill-opacity: 1;}
        .jssorb053 .i.idn {opacity:.3;}

        .jssora093 {display:block;position:absolute;cursor:pointer;}
        .jssora093 .c {fill:none;stroke:#fff;stroke-width:400;stroke-miterlimit:10;}
        .jssora093 .a {fill:none;stroke:#fff;stroke-width:400;stroke-miterlimit:10;}
        .jssora093:hover {opacity:.8;}
        .jssora093.jssora093dn {opacity:.6;}
        .jssora093.jssora093ds {opacity:.3;pointer-events:none;}
        @media (min-width: 768px) and (max-width: 1200px) {
          .navbar-nav .open .dropdown-menu {
            position: static;
            float: none;
            width: auto;
            margin-top: 0;
            background-color: transparent;
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
          }
          .navbar-nav .open .dropdown-menu > li > a {
            line-height: 20px;
          }
          .navbar-nav .open .dropdown-menu > li > a,
          .navbar-nav .open .dropdown-menu .dropdown-header {
            padding: 5px 15px 5px 25px;
          }
          .dropdown-menu > li > a {
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: normal;
            line-height: 1.42857143;
            color: #f1c152 !important;
            white-space: nowrap;}
            .navbar-header {
                float: none;
            }
            .navbar-toggle {
                display: block;
            }
            .navbar-collapse {
                border-top: 1px solid transparent;
                box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
            }
            .navbar-collapse.collapse {
                display: none!important;
            overflow-y: auto !important;
            }
            .navbar-nav {
                float: none!important;
                /*margin: 7.5px -15px;*/
            margin: 7.5px 50px 7.5px 0px
            }
            .navbar-nav>li {
                float: none;
            }
            .navbar-nav>li>a {
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .navbar-text {
                float: none;
                margin: 15px 0;
            }
            /* since 3.1.0 */
            .navbar-collapse.collapse.in { 
                display: block!important;
            }
            .collapsing {
                overflow: hidden!important;
            }
        }
        .cont-far{width:97%!important; max-width:84%; margin:auto;}
        @media(min-width:300px) and (max-width:767px){
        .cont-far{width:93%; max-width:93%; margin:auto;}
        }
        .slider_btm_heading{
          text-align: center !important;
          color: #fff;
        }
    </style>  
  </head>
<body>
  <div class="container-fluid">
    <div class="row">
     <div class="col-md-12 col-sm-12 box-nav-gray">
          <img src="https://www.dawateislami.net/images/khutba.svg" class="khutba-slogan img-responsive" alt="Dawat-e-Islami">
        </div>
        <div class="container">
          <div class="row hidden-xs hidden-is hidden-sm header-banner">
            <div class="col-md-2 col-sm-2">
              <img src="{{asset('assets/frontend/images/logo_0.png')}}" alt="idara raza e mujtaba" class="img-responsive logo-header">
            </div>
            <div class="col-md-8 col-sm-8">
              <!-- <img src="https://www.dawateislami.net/images/motto-urdu-30px.svg" class="img-responsive svg img-center motto-urdu m-urdu" alt="Madani Maqsad">
              <img src="https://www.dawateislami.net/images/motto-en-30p-2.svg" class="img-responsive svg img-center motto-eng m-en hidden" alt="Madani Maqsad"> -->
            </div>
            <div class="col-md-2 col-sm-2">
        <p class="text-center"><span class="h2">19</span> <br>Muharram ul Haram<br>1440 Hijri</p>
        <!-- <p class="text-center"><span class="h2">30</span> <br>Muharram ul Haram<br>1440 Hijri</p> -->
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row nav-block box-nav-gray">
            <nav class="navbar navbar-main navbar-inverse" role="navigation">
              <div class='cont-far'>
                <div class="row">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hidden-md hidden-lg" href="#">
                      <!-- <img src="{{asset('assets/frontend/images/logo-white.png')}}" alt="logo" class="img-responsive logo"> -->
                    </a>
                    <select class="form-control hidden-md hidden-lg" style="width: 30%;color: black;margin-left: 20%;" id="site_lang" name="site_lang" onchange="loadSite(this.options[this.selectedIndex].value);">
                      <option value="">Global</option>
                      <option value="/ar">العربية</option>
                    </select>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="navbar">
              <ul class="nav navbar-nav">
                <li class="active"><a href="javascript:void(0)">Home</a></li>
                <li><a href="javascript:void(0)" target="_blank">Al-Quran</a></li>
                      <li><a href="javascript:void(0)">Books</a></li>
                <li> <a href="javascript:void(0)">Media</a> </li>
                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Donation<i class="fa fa-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                          <li><a href="javascript:void(0)">Why Donate to us?</a></li>
                <li><a href="javascript:void(0)">Donate Now</a></li>
                        </ul>
                </li>
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Departments <i class="fa fa-chevron-down"></i></a>
              <ul class="dropdown-menu">
                  <li><a href="javascript:void(0)">Farz Uloom</a></li>
                  <li><a href="javascript:void(0)">Islamic Sisters</a></li>
                  <li><a href="javascript:void(0)">Madrasa-tul-Madina Online</a></li>
                  <li><a href="javascript:void(0)">Hajj-o-Umrah</a></li>
                 <!-- <li><a href="https://departments.dawateislami.net/toqeet/">Majlis Tauqeet</a></li>-->
                  <li><a href="javascript:void(0)">Tajheez-o-Takfeen</a></li>
                  <li><a href="javascript:void(0)">Tawizat-e-Attaria</a></li>
                  <li><a href="javascript:void(0)">Ahkam-e-Hajj</a></li>
                  <li><a href="javascript:void(0)">Social Media</a></li>
              </ul>
              </li>
                   <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">More<i class="fa fa-chevron-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)">Social Activities</a></li>
                            <li><a href="/gallery/">Gallery</a></li>
                            <li><a href="javascript:void(0)">Downloads</a></li>
                            <li><a href="javascript:void(0)">Location</a></li>
                            <li class="visible-sm"><a href="javascript:void(0)">Prayer Timings</a></li>
                            <li class="visible-sm"><a href="javascript:void(0)">Ruhani Ilaj</a></li>
                        </ul>
                      </li>
                    </ul>
                <ul class="nav navbar-nav" style="float:right;margin-top:3px;">
                  <li>
                  <select class="form-control hidden-xs hidden-sm" id="site_lang" name="site_lang" onchange="loadSite(this.options[this.selectedIndex].value);">
                    <option value="">Global</option>
                    <option value="/ar">العربية</option>
                  </select>
                  </li>
                </ul>
                </div><!-- /.navbar-collapse -->
              </div>
            </div>
          </nav>
        </div>
      <div class="row quicklinks">
          <nav class="navbar navbar-inverse ql-navbar ql-opaque">
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12 hidden-xs ">
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                </div>
              </div>
            </div>
          </nav>
       </div>
    </div>     
<script>
  function loadSite(site) {
    if(site != null && site.trim().length > 0) {
      window.location = window.location.origin + site;
    }
  }
 </script>
   </div>
 </div>     
<div class="container-fluid content">
<!-- <Slider start>-->                 
<div class="container">
  <section class="row">
        <div class="col-md-12 col-sm-12 col-is-12 col-xs-12">
          <div class="panel panel-default card-lg">
          <div class="panel-body card">
            <div class="card-image" >
                <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:433px;overflow:hidden;visibility:hidden;">
                  <!-- Loading Screen -->
                  <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                      <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{{asset('assets/frontend/images/spin.svg')}}" />

                  </div>
                  <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
                      <div>
                          <img data-u="image" src="{{asset('assets/frontend/images/slider/banner_1.jpg')}}"  style="" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('assets/frontend/images/slider/banner_1.jpg')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('assets/frontend/images/slider/banner_1.jpg')}}" />
                      </div>
                      <div>
                          <img data-u="image" src="{{asset('assets/frontend/images/slider/banner_1.jpg')}}" />
                      </div>
                  </div>
              <!-- Bullet Navigator -->
              <div data-u="navigator" class="jssorb053" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                  <div data-u="prototype" class="i" style="width:16px;height:16px;">
                      <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                          <path class="b" d="M11400,13800H4600c-1320,0-2400-1080-2400-2400V4600c0-1320,1080-2400,2400-2400h6800 c1320,0,2400,1080,2400,2400v6800C13800,12720,12720,13800,11400,13800z"></path>
                      </svg>
                  </div>
              </div>
        <!-- Arrow Navigator -->
             <!--  <div data-u="arrowleft" class="jssora093" style="width:50px;height:50px;top:0px;left:30px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                  <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                      <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                      <polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
                      <line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
                  </svg>
              </div>
              <div data-u="arrowright" class="jssora093" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                  <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                      <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                      <polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
                      <line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
                  </svg>
              </div> -->
          </div>
          <script type="text/javascript">jssor_1_slider_init();</script>
            <!-- <a href="https://www.dawateislami.net/news/ilyas-qadri-updates/dua-e-attar-for-booklet-wudu-aur-science"><img src="https://data2.dawateislami.net/static/images/main-website/promotional-banner/298-dua-e-attar-wudu-aur-science.jpg" class="img-block-responsive wide"></a> -->
              </div>
            </div>
          <div class="panel-footer slider_btm_heading">
            <span class="card-description h5">
                Idara Raza e Mujtaba
          </span>
          </div>
        </div>
      </div>
         <!-- start  -->
         <div class="row">
              <div class="col-md-6 col-sm-offset-5">
                  <h3>Latest video speech</h3>
              </div>
         </div>
          <div class="col-md-12 col-sm-12 col-is-12 col-xs-12">
              <ul id="content-slider" class="content-slider">
                  <li>
                      <img style="height: 288px;" src="https://i.ytimg.com/vi/DtvlroYYRYM/hqdefault.jpg">
                  </li>
                  <li>
                       <img style="height: 288px;" src="https://i.ytimg.com/vi/TjthxY4gwSM/hqdefault.jpg">
                  </li>
                  <li>
                      <img style="height: 288px;" src="https://i.ytimg.com/vi/PX4i0a6ArDY/hqdefault.jpg">
                  </li>
                  <li>
                      <img style="height: 288px;" src="https://i.ytimg.com/vi/PX4i0a6ArDY/hqdefault.jpg">
                  </li>
                  <li>
                       <img style="height: 288px;" src="https://i.ytimg.com/vi/PX4i0a6ArDY/hqdefault.jpg">
                  </li>
                  <li>
                       <img style="height: 288px;" src="https://i.ytimg.com/vi/PX4i0a6ArDY/hqdefault.jpg">
                  </li>
                   <li>
                       <img style="height: 288px;" src="https://i.ytimg.com/vi/PX4i0a6ArDY/hqdefault.jpg">
                  </li>
                   <li>
                       <img style="height: 288px;" src="https://i.ytimg.com/vi/PX4i0a6ArDY/hqdefault.jpg">
                  </li>
                 
              </ul>
            </div>
     </section>
     <div class="row">
        <div class="col-md-6 col-sm-offset-5">
              <h3>Latest Programs </h3>
        </div>
     </div>
     <section class="row">
        <div class="col-md-6 col-sm-12 col-is-12 col-xs-12">
            <div class="panel panel-default card-lg">
            <div class="panel-body card">
              <div class="card-image">
                <a href="javascript:void(0)"><img src="{{asset('assets/frontend/images/qari_sb_01.png')}}" class="img-block-responsive wide"></a>
                </div>
              </div>
              <div class="panel-footer">
                <span class="card-description h5"><a class="link-white-hover" href="javascript:void(0)">Mukhtalif Shehron Ka Jadwal</a>
              </span>
            </div>
            </div>
          </div>
              <div class="col-md-3 col-sm-6 col-is-6 col-xs-12" >
              <div class="panel panel-default card-md">
              <div class="panel-body card">
                <div class="card-image"><a href="javascript:void(0)"><img src="https://data2.dawateislami.net/static/images/main-website/banners/736-faizan-e-molana-jami.jpg" class="img-block-responsive wide"></a>
                </div>
              </div>
              <div class="panel-footer">
                <span class="card-description h5"><a class="link-white-hover" href="https://www.dawateislami.net/medialibrary/14428">Youm-e-Urs</a></span>
              </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-is-6 col-xs-12">
              <div class="panel panel-default card-md">
              <div class="panel-body card">
              <div class="card-image"><a href="javascript:void(0)"><img src="https://data2.dawateislami.net/static/images/main-website/banners/414-faizan-e-mufti-farooq-attari.jpg" class="img-block-responsive wide"></a>
                </div>
              </div>
              <div class="panel-footer">
                <span class="card-description h5"><a class="link-white-hover" href="javascript:void(0)">Youm-e-Urs</a></span>
              </div>
              </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-is-6 col-xs-12">
              <div class="panel panel-default card-md">
              <div class="panel-body card">
                <div class="card-image"><a href="javascript:void(0)"><img src="https://i.ytimg.com/vi/TjthxY4gwSM/hqdefault.jpg" class="img-block-responsive wide"></a>
                </div>
              </div>
              <div class="panel-footer">
                <span class="card-description h5"><a class="link-white-hover" href="javascript:void(0)">Wo Aik Hi Hay Bas</a></span>
              </div>
              </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-is-6 col-xs-12">
              <div class="panel panel-default card-md">
              <div class="panel-body card">
                <div class="card-image"><a href="javascript:void(0)"><img src= "https://data2.dawateislami.net/static/images/main-website/banners/515-prayer-time-applicatin.jpg" class="img-block-responsive wide"></a>
                </div>
              </div>
              <div class="panel-footer">
                <span class="card-description h5"><a class="link-white-hover" href="javascript:void(0)">Prayer Times Application</a></span>
              </div>
              </div>
            </div>
            
          </section>
      </section>
    </div>
<!-- </Slider end>-->
<!-- <Latest Book start>-->
<section class="row box-green books" data-type="background" data-speed="10" style="max-width:  none;">
   <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h3><i class="fa fa-book color-scheme-2-txt"></i> Featured Books</h3>
          </div>
          <div class="clearfix"></div>
                  <div class="col-md-3 col-sm-3 col-xs-6 book-item margin-tb-10">
            <a href="/bookslibrary/3114" title="Nekiyan Chupao"><img src="https://data2.dawateislami.net/Data/Books/Read/ur/2016/1592/bt1592.jpg" alt="Nekiyan Chupao" class="img-responsive img-center">
              <h4 class="text-center color-scheme-2-txt">Nekiyan Chupao</h4></a>
          </div>

          <div class="col-md-3 col-sm-3 col-xs-6 book-item margin-tb-10">
            <a href="javascript:void(0)" title="Waliullah Ki Pehchan"><img src="https://data2.dawateislami.net/Data/Books/Read/ur/2016/1496/bt1496.jpg" alt="Waliullah Ki Pehchan" class="img-responsive img-center">
              <h4 class="text-center color-scheme-2-txt">Waliullah Ki Pehchan</h4></a>
          </div>

                  <div class="col-md-3 col-sm-3 col-xs-6 book-item margin-tb-10">
            <a href="javascript:void(0)" title="Rafiq-e-Safar Kesa Ho?"><img src="https://data2.dawateislami.net/Data/Books/Read/ur/2018/1742/bt1742.jpg" alt="Rafiq-e-Safar Kesa Ho?" class="img-responsive img-center">
              <h4 class="text-center color-scheme-2-txt">Rafiq-e-Safar Kesa Ho?</h4></a>
          </div>

                  <div class="col-md-3 col-sm-3 col-xs-6 book-item margin-tb-10">
            <a href="/bookslibrary/354" title="Mufti e Dawateislami"><img src="https://data2.dawateislami.net/Data/Books/Read/ur/2006/158/bt158.jpg" alt="Mufti e Dawateislami" class="img-responsive img-center">
              <h4 class="text-center color-scheme-2-txt">Mufti e Dawateislami</h4></a>
      </div>       
      <div class="clearfix"></div>
          <div class="col-md-12">
            <ul class="pager">
              <li><a href="/bookslibrary" class="btn-lg">Latest Books <span aria-hidden="true">&rarr;</span></a></li>
            </ul>
          </div>
        </div>
      </div>
</section>          <!-- </Latest end>--> 
<!-- <Featured start>-->
<section class="row featured">
      <div class="container">
        <div class="row">
        <div class="col-md-12">
          <h3 class="color-scheme-1-txt"><i class="fa fa-puzzle-piece"></i> Featured Sections</h3>
        </div>
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover"  href="javascript:void(0)" target="_blank">
          <div class="panel-body card">
            <div class="card-image">
              <img src="{{asset('assets/frontend/img/featured/quran-e-kareem.jpg')}}" class="img-block-responsive" alt="Quran E Kareem" />
            </div>
          </div>
          <div class="panel-footer color-scheme-1-bg">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">Quran E Kareem</span>
            </span>
          </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover"  href="http://aboutmuhammad.net/">
          <div class="panel-body card">
            <div class="card-image">
              <img src="{{asset('assets/frontend/img/featured/about-sarkar.jpg')}}" class="img-block-responsive" alt="Holy Prophet صلی اللہ علیہ وسلم" />
            </div>
          </div>
          <div class="panel-footer color-scheme-1-bg">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">The Holy Prophet صلی اللہ علیہ واٰلہ وسلم​</span>
            </span>
          </div>
          </a>
        </div>
       
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover"  href="javascript:void(0)">
          <div class="panel-body card">
            <div class="card-image">
              <img src="{{asset('assets/frontend/img/featured/media-library.jpg')}}" class="img-block-responsive" alt="Media Library" />
            </div>
          </div>
          <div class="panel-footer color-scheme-1-bg">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">Media Library</span>
            </span>
          </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover"  href="http://www.ilyasqadri.com/">
          <div class="panel-body card">
            <div class="card-image">
              <img src="https://i.ytimg.com/vi/DtvlroYYRYM/hqdefault.jpg" class="img-block-responsive" alt="Ameer e Ahle Sunnat" />
            </div>
          </div>
          <div class="panel-footer color-scheme-1-bg">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">Ameer e Ahle Sunnat</span>
            </span>
          </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover"  href="/bookslibrary">
          <div class="panel-body card">
            <div class="card-image">
              <img src="{{asset('assets/frontend/img/featured/book-library.jpg')}}" class="img-block-responsive" alt="Book Library" />
            </div>
          </div>
          <div class="panel-footer">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">Book Library</span>
            </span>
          </div>
          </a>
        </div>
        <div class="col-md-4 col-sm-6 col-is-6 col-xs-12 col-sm-offset-0 margin-tb-15 featured-item">
          <a class="link-white-hover" href="/gallery">
          <div class="panel-body card">
            <div class="card-image">
              <img src="{{asset('assets/frontend/img/featured/image-gallery.jpg')}}" class="img-block-responsive" alt="Image Gallery" />
            </div>
          </div>
          <div class="panel-footer color-scheme-1-bg">
            <span class="card-description">
              <span class="h4 text-center color-scheme-2-txt">Image Gallery</span>
            </span>
          </div>
          </a>
        </div>
        </div>
      </div>

    </section>          <!-- </Featured end>-->      
        <!-- <About Us start>-->
         <section class="row about">
        <div class="col-md-8 col-md-offset-2">
          <h3 class="text-center">About Idara Razae e Mujtaba</h3>
          <blockquote>
            <a href="http://www.dawateislami.net" class="link-white-hover"></a> Idara Raza e Mujtaba is a global, non-political and peaceful movement for the preaching of the Holy Quran and Sunnah. It has established more than 10 departments endeavoring to promote Islamic teachings. The official website of raza e mujtaba is enriched with precious treasure of Islamic Knowledge which has proved as second to none in reforming the Muslim Ummah.... <a href="javascript:void(0)" class="link-white-hover"><b>continue reading</b></a>    
          </blockquote>
        </div>
</section>    <!-- </About Us end>-->
        
        <!-- <Footer start>-->
<section class="row footer"> 
   <div class="col-md-6 col-sm-5 col-xs-12 border-b-2">
      <ul id="social-service">
            <li><a href="https://twitter.com/idararazaemujtaba" target="_blank"><img src="{{asset('assets/frontend/images/twitter.png')}}" alt="Share twitter"></a></li>
            <li><a href="https://plus.google.com/+razaemujtaba" target="_blank"><img src="{{asset('assets/frontend/images/google.png')}}" alt="Share google"></a></li>
            <li><a href="https://www.facebook.com/dawateislami.net" target="_blank"><img src="{{asset('assets/frontend/images/facebook.png')}}" alt="Share facebook"></a></li>
      </ul>
        </div>
        <div class="col-md-6 col-sm-7 col-xs-12 connect border-b-2">
      <h4 class="col-md-offset-3 col-sm-offset-2 color-scheme-2-txt">Connect to Idara e Razae Mujtaba</h4>
        <div class="col-md-6 col-sm-6 col-xs-12">
          
          <span><strong>Phone: </strong><a href="tel:+922134921388">(+92)21-34921388-93</a></span> <br>
          <span><strong>UAN: </strong><a href="tel:+9221111252692">(+92)21-111-25-26-92</a></span> <br>
          <span><strong>Email: </strong><a href="mailto:support@razaemujtaba.com">support@razaemujtaba.com</a></span>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
        
          <address>
             <strong>Address: </strong>   Rasol Pora,
                Madina Masjid, <br> Near Capital Telephone Exchange, <br>
                Industries Road, <br>
                Faisalabad, <br>
                Pakistan <br>
              </address>
        </div>
        </div>
    </section>
<!-- </Footer end>--> 
<section class="row copyright">
      <div class="col-md-12">
        <h5 class="text-center">Copyright &copy; <span>2018</span> by Idara Raza e Mujtaba</h5>
      </div>
</section>
<!-- end -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $("#content-slider").lightSlider({
        item: 3,
        autoWidth: false,
        slideMove: 1, // slidemove will be 1 if loop is true
        // controls: false,
    }); 

  });
</script>
  <script src="{{asset('assets/frontend/js/layout.js')}}"></script>
  <script src="{{asset('assets/frontend/js/jquery-ui.js')}}"></script>
  <script src="{{asset('assets/frontend/js/storyjs-embed.js')}}"></script>
  <script src="{{asset('assets/frontend/js/jquery.serializejson.min.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
  <!-- <script type="text/javascript">jssor_1_slider_init();</script> -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js" integrity="sha256-nHmCK+HOPMPezzS3ky9VKznMWH4sW4keT8HrMaDNbYo=" crossorigin="anonymous"></script>
<script type="text/javascript">

  
$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
        items:1,
        merge:true,
        loop:true,
        margin:10,
        video:true,
        lazyLoad:true,
        center:true,
        responsive:{
            480:{
                items:2
            },
            600:{
                items:4
            }
        }
    })
});
       
$('#btn_search').click(function() {
  searchContent();
});
  
function searchContent() {
  if($('#txtSearch').val() == null || $('#txtSearch').val().trim().length <= 0) {
    return false;
  }
  $('.searchResultspinner').show();
  var searchForm = $("#search_form");
  var serializedData = JSON.stringify(searchForm.serializeJSON());
    $.ajax({
    url : '/search',
    type : 'POST',
    data: serializedData,
    contentType: 'application/json',
    success : function(data) {
      $("#searchResults").html(data);
      $('.searchResultspinner').hide();
    },
    error : function() {
      console.log("Some error there.");
    }
  });
}

function paginationCall(page) {
  $("#pageNumber").val(page);
  searchContent();
  return false; 
}
</script>
<script src="{{asset('assets/frontend/js/main.js')}}"></script>
</body>
</html>
  

